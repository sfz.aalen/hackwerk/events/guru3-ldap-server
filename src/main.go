package main

import (
	"database/sql"
	"fmt"
	"log"
	"os"
	"os/signal"
	"regexp"
	"strings"
	"syscall"

	"github.com/lor00x/goldap/message"
	_ "github.com/lib/pq"
	ldap "github.com/vjeantet/ldapserver"
)

var db *sql.DB

func main() {
	//Create a new LDAP Server
	server := ldap.NewServer()

	//Create routes bindings
	routes := ldap.NewRouteMux()
	routes.NotFound(handleNotFound)
	routes.Abandon(handleAbandon)
	routes.Bind(handleBind)

	routes.Search(handleSearch).Label("Search - Generic")

	//Attach routes to server
	server.Handle(routes)

	var err error
	connStr := "postgres://"+os.Getenv("pg_user")+":"+os.Getenv("pg_pw")+"@"+os.Getenv("pg_host")+"/"+os.Getenv("pg_db")
	db, err = sql.Open("postgres", connStr)
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()

	// listen on 10389 and serve
	go server.ListenAndServe("0.0.0.0:10389")

	// When CTRL+C, SIGINT and SIGTERM signal occurs
	// Then stop server gracefully
	ch := make(chan os.Signal)
	signal.Notify(ch, syscall.SIGINT, syscall.SIGTERM)
	<-ch
	close(ch)

	server.Stop()
}

func handleNotFound(w ldap.ResponseWriter, r *ldap.Message) {
	switch r.ProtocolOpType() {
	case ldap.ApplicationBindRequest:
		res := ldap.NewBindResponse(ldap.LDAPResultSuccess)
		res.SetDiagnosticMessage("Default binding behavior set to return Success")

		w.Write(res)

	default:
		res := ldap.NewResponse(ldap.LDAPResultUnwillingToPerform)
		res.SetDiagnosticMessage("Operation not implemented by server")
		w.Write(res)
	}
}

func handleAbandon(w ldap.ResponseWriter, m *ldap.Message) {
	var req = m.GetAbandonRequest()
	// retreive the request to abandon, and send a abort signal to it
	if requestToAbandon, ok := m.Client.GetMessageByID(int(req)); ok {
		requestToAbandon.Abandon()
		//log.Printf("Abandon signal sent to request processor [messageID=%d]", int(req))
	}
}

func handleBind(w ldap.ResponseWriter, m *ldap.Message) {
	r := m.GetBindRequest()
	res := ldap.NewBindResponse(ldap.LDAPResultSuccess)
	if r.AuthenticationChoice() == "simple" {
		w.Write(res)
		return
	} else {
		res.SetResultCode(ldap.LDAPResultUnwillingToPerform)
		res.SetDiagnosticMessage("Authentication choice not supported")
	}

	w.Write(res)
}

func stringToKeypadNum(in string) (nums string) {
	in = strings.ToLower(in)
	for _, ch := range in {
		chs := fmt.Sprintf("%c", ch)
		switch chs {
		case "a", "b", "c":
			nums += "2"
		case "d", "e", "f":
			nums += "3"
		case "g", "h", "i":
			nums += "4"
		case "j", "k", "l":
			nums += "5"
		case "m", "n", "o":
			nums += "6"
		case "p", "q", "r", "s":
			nums += "7"
		case "t", "u", "v":
			nums += "8"
		case "w", "x", "y", "z":
			nums += "9"
		}
	}
	return
}

func doDBSearch(searchexp string) [][]string {
	//println("select extension,name,location from core_extension WHERE \"inPhonebook\" IS TRUE AND event_id = 1 AND " + searchexp)
	rows, err := db.Query("select extension,name,location from core_extension WHERE \"inPhonebook\" IS TRUE AND event_id = 1 AND " + searchexp)
	if err != nil {
		log.Fatal(err)
	}
	defer rows.Close()
	ret := make([][]string, 0)
	for rows.Next() {
		var extension string
		var name string
		var location string
		err = rows.Scan(&extension, &name, &location)
		if err != nil {
			log.Fatal(err)
		}
		next := make([]string, 3)
		next[0] = extension
		next[1] = name
		next[2] = location
		ret = append(ret, next)
		//fmt.Println(extension, name, eventid)
	}
	err = rows.Err()
	if err != nil {
		log.Fatal(err)
	}
	return ret
}

func handleSearch(w ldap.ResponseWriter, m *ldap.Message) {
	r := m.GetSearchRequest()
	/*log.Printf("Request BaseDn=%s", r.BaseObject())
	log.Printf("Request Filter=%s", r.Filter())
	log.Printf("Request FilterString=%s", r.FilterString())
	log.Printf("Request Attributes=%s", r.Attributes())
	log.Printf("Request TimeLimit=%d", r.TimeLimit().Int())*/

	// Extract sn value out of LDAP filter with the help of regex
	rre := regexp.MustCompile("\\(sn=([^\\*]*)\\*\\)")
	matches := rre.FindStringSubmatch(r.FilterString())
	if len(matches) < 2 {
		println("No matches")
		return
	}
	search := strings.ToLower(matches[1])

	// First search for the name in the keypad if the number is four digits long
	if len(search) == 4 {
		db := doDBSearch("extension = '" + stringToKeypadNum(search) + "'")
		if len(db) > 0 {
			number := db[0][0]
			name := db[0][1]
			e := ldap.NewSearchResultEntry("cn=" + number + ", " + string(r.BaseObject()))
			e.AddAttribute("sn", message.AttributeValue(number+" - "+name))
			e.AddAttribute("telephoneNumber", message.AttributeValue(number))
			w.Write(e)
		}
	}

	// Then search for the name in the DB
	result := doDBSearch("name LIKE '%" + search + "%'")
	if len(result) > 0 {
		for _, entry := range result {
			number := entry[0]
			name := entry[1]
			e := ldap.NewSearchResultEntry("cn=" + number + ", " + string(r.BaseObject()))
			e.AddAttribute("sn", message.AttributeValue(name))
			e.AddAttribute("telephoneNumber", message.AttributeValue(number))
			w.Write(e)
		}
	}

	// Then search for the location in the DB
	result = doDBSearch("location LIKE '%" + search + "%'")
	if len(result) > 0 {
		for _, entry := range result {
			number := entry[0]
			name := entry[1]
			location := entry[2]
		e := ldap.NewSearchResultEntry("cn=" + number + ", " + string(r.BaseObject()))
			e.AddAttribute("sn", message.AttributeValue(name+" in "+location))
			e.AddAttribute("telephoneNumber", message.AttributeValue(number))
			w.Write(e)
		}
	}

	// Handle Stop Signal (server stop / client disconnected / Abandoned request....)
	select {
	case <-m.Done:
		log.Print("Leaving handleSearch...")
		return
	default:
	}

	/*e = ldap.NewSearchResultEntry("cn=Claire Thomas, " + string(r.BaseObject()))
	e.AddAttribute("mail", "claire.thomas@gmail.com")
	e.AddAttribute("cn", "Claire THOMAS")
	w.Write(e)*/

	//attributes='sn givenName telephoneNumber mobile homePhone'

	res := ldap.NewSearchResultDoneResponse(ldap.LDAPResultSuccess)
	w.Write(res)

}
