module a2/poc/guru-ldap

go 1.15

require (
	github.com/lib/pq v1.10.9
	github.com/lor00x/goldap v0.0.0-20180618054307-a546dffdd1a3
	github.com/vjeantet/ldapserver v1.0.1
)
